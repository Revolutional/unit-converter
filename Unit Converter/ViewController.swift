//
//  ViewController.swift
//  Unit Converter
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 ITE. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate {

    let userDefaultsLastRowKey = "defaultCelsiusPickerRow"
    
    private var temperatureValues = [Int]();
    private var converter = UnitConverter();
    
    @IBOutlet var TemperatureRange: temperatureRange!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var celsiusPicker: UIPickerView!
    
    func initialPickerRow() -> Int
    {
        let savedRow = NSUserDefaults.standardUserDefaults().objectForKey(userDefaultsLastRowKey) as? Int
        
        if let row = savedRow
        {
            return row
        }
        
        else
        {
            return celsiusPicker.numberOfRowsInComponent(0) / 2
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //for index in -100..<100
        //{
            //temperatureValues.append(index);
        //}
        let defaultPickerRow = celsiusPicker.numberOfRowsInComponent(0)/2;
        celsiusPicker.selectRow(defaultPickerRow, inComponent: 0, animated: false);
        pickerView(celsiusPicker, didSelectRow: defaultPickerRow, inComponent: 0);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pickerView(pickerView: UIPickerView,titleForRow row: Int, forComponent component:Int) -> String?
    {
        let celciusValue = TemperatureRange.values[row]
        return "\(celciusValue)℃"
    
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        saveSelectedRow(row)
        displayConvertedTemperatureForRow(row)
        
    }
    
    func saveSelectedRow(row:Int)
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(row, forKey: userDefaultsLastRowKey)
        defaults.synchronize()
    }
    
    func displayConvertedTemperatureForRow(row:Int)
    {
        let degreesCelsius = Int(TemperatureRange.values[row])
        temperatureLabel.text = "\(Int(converter.degreesFahrenheit(degreesCelsius)))℉"
    }

}

