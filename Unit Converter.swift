//
//  Unit Converter.swift
//  Unit Converter
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 ITE. All rights reserved.
//

import Foundation

class UnitConverter
{
    func degreesFahrenheit(degreeCelsius: Int) -> Int
    {
        return Int(1.8 * Float(degreeCelsius) + 32.0)
    }
}