//
//  TemperatureRange.swift
//  Unit Converter
//
//  Created by GDD Student on 10/5/16.
//  Copyright © 2016 ITE. All rights reserved.
//

import UIKit

class temperatureRange: NSObject, UIPickerViewDataSource
{
    let values = (-100...100).map{$0};
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return values.count;
        //return 10;
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return 1;
    }
    
}